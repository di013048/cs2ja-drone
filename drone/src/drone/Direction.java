package drone;

import java.util.Random;

public enum Direction {
	// Set elements to Direction enum
	NORTH,
	EAST,
	SOUTH,
	WEST;
	
	// Selects random direction from Direction enum
	public static Direction getRandomDirection() {
		// Initialise random
        Random random = new Random();
        // Return random direction from Direction enum
        return values()[random.nextInt(values().length)];
    }
	
	// Selects next direction clockwise from previous direction
	public Direction getNext(Direction direction) {
		// Change direction to clockwise direction
		switch (direction) {
	      case NORTH:
	    	  direction = Direction.EAST;
              break;
	      case EAST:
	    	  direction = Direction.SOUTH;
              break;
	      case SOUTH:
	    	  direction = Direction.WEST;
              break;
	      case WEST:
	    	  direction = Direction.NORTH;
              break;
	    }
		return direction;
	}
	
	public static void main(String[] args) {
		System.out.println(getRandomDirection());
	}
}