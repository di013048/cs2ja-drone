package drone;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import java.util.List;
/**
 * Simple program to show arena with multiple drones
 * @author shsmchlr
 *
 */
public class DroneInterface {
	/**
	/**
	* Display the drone arena on the console
	*
	*/
	private Scanner s;								// scanner used for input from user
    private DroneArena myArena;				// arena in which drones are shown
    
	void doDisplay() {
		// Initialise c object with x and y size
		ConsoleCanvas c = new ConsoleCanvas(myArena.getxSize(), myArena.getySize());
		// Call to showDrones method with c object
		myArena.showDrones(c);
		// Call to method toString with c object
		System.out.println(c.toString());
	}
	
	// Move all drones once
	void moveDronesOneTime() {
		// Call to move all drones method using myArena as object
		myArena.moveAllDrones(myArena);
		// Display drones
		doDisplay();
		// Display drone positions
		System.out.print(myArena.toString());
	}
	
	// Move all drones 10 times
	void moveDronesTenTime(int moveIteration) {
		// Iterate 10 times
		for (int i = 0; i < moveIteration; i++) {
			// Call to move all drones once
			moveDronesOneTime();
			try {
				// Sleep for 200
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	// Method to create a new building
	public DroneArena newArena() {
		// Listen to user input
		s = new Scanner(System.in);
		try {
			System.out.print("Enter the arena size as 'x y': ");
			// Take next next as input
			String arenaSize = s.nextLine();
			// Split input into 2 variables
			String[] integers = arenaSize.trim().split("\\s+");
			int x = Integer.parseInt(integers[0]);
			int y = Integer.parseInt(integers[1]);
			// If x or y is less than 3 provide error
			if (x < 3 || y < 3) {
				throw new Exception("Needs to be greater than 3");
			}
			// Set arena to new drone arena size
			this.myArena = new DroneArena(x,y);
			return myArena;
		} catch (Exception error) {
			// Catch error and apply default drone arena size of 3 by 33
			System.out.println("Default x value too small, default size implemented");
			this.myArena = new DroneArena(3,3);
			return myArena;
		}
	}
	
	// Method to load arena
	public void loadArenaMenu() throws IOException {
		// Initialise JFileChooser object
		JFileChooser fileDialog = new JFileChooser(FileSystemView.getFileSystemView());
		// Initialise File object with current path + ArenaStorage
		File arenaDirectory = new File(System.getProperty("user.dir"),  "ArenaStorage");
		// Set title to choose a building to load
		fileDialog.setDialogTitle("Choose an arena to load");
		// Set file selection to files only
		fileDialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
		// set file explorer directory
		fileDialog.setCurrentDirectory(arenaDirectory);
		// Open file dialog
        int fileChoice = fileDialog.showOpenDialog(null);
        // If approve button is selected
        if (fileChoice == JFileChooser.APPROVE_OPTION) {
        	// Initialise file object with selected file
            File file = fileDialog.getSelectedFile();
            // Create scanner object
            try (Scanner s = new Scanner(new BufferedReader(new FileReader(file)))) {
            	BufferedReader fileReader = new BufferedReader(new FileReader(file));
            	// Initialise lines variable as 0
            	int lines = 0;
            	// While file line is not empty
            	while (fileReader.readLine() != null ) {
            		// Increment lines variable
            		lines++;
            	}
            	// Close file reader
            	fileReader.close();
            	// Read first line of file
            	String text = Files.readAllLines(Paths.get(file.getAbsolutePath())).get(0);
            	// Split line into 2 variables
            	String[] parts = text.split(",");
            	// Initialise arenaSizeX as first integer
            	int arenaSizeX = Integer.parseInt(parts[0]);
            	// Initialise arenaSizeY as second integer
            	int arenaSizeY = Integer.parseInt(parts[1]);
            	// Initialise myArena object by calling to DroneArena class with new arena size
            	myArena = new DroneArena(arenaSizeX, arenaSizeY);
            	// For each line in file
            	for(int i = 1; i < lines; i++) {
            		// Initialise drone string as line content
            		String drone = Files.readAllLines(Paths.get(file.getAbsolutePath())).get(i);
            		// Initialise array as removed unnecessary information from string
            		String[] splitted = drone.replaceAll("Drone " + (i-1) , "").replaceAll(" is at ",  "").replaceAll(" and the direction is ", ",").split(",");
            		// Initialise x as first integer in string array
            		int x = Integer.parseInt(splitted[0]);
            		// Initialise y as second integer in string array
            		int y = Integer.parseInt(splitted[1]);
            		// Initialise direction as third integer in string array
            		String direction = splitted[2];
            		// Call to method getRandomDirection using Direction object
            		Direction dir = Direction.getRandomDirection();
            		// Set dir to direction based on direction variable from file
            		switch(direction) {
                    case "North":
                    	dir = Direction.NORTH;
                    	break;
                    case "East":
                    	dir = Direction.EAST;
                    	break;
                    case "South":
                    	dir = Direction.SOUTH;
                    	break;
                    case "West":
                    	dir = Direction.WEST;
                    	break;
            		default:
            			break;
            		}
            		// Initialise Adder object by calling to Drone with x, y and dir
            		Drone Adder = new Drone(x, y, dir);
            		// Arraylist add functionality parsing Adder using myArena object
            		myArena.drones.add(Adder);
                }
            }
        }
    }
    
	// Method to save arena menu
	public void saveArenaMenu() throws IOException {
		// Listen to user input
		s = new Scanner(System.in);
		// Ask for user input
		System.out.println("Please enter the name of the arena you would like to save: ");
		// Read user input
		String fileName = s.nextLine();
		// Check for symbols or special characters
		if (fileName.matches("^[a-zA-Z]*$")) {
			PrintWriter out;
			// Create out object using print writer with file location
			out = new PrintWriter(new BufferedWriter(new FileWriter("ArenaStorage/" + fileName + ".txt")));
			// Output x and y arena size to first line in file
			out.print(myArena.getxSize() + "," + myArena.getySize() + "\n");
			// For each drone in list
			for (Drone element : myArena.drones) {
				// Output drone list element to new line
				out.write(element.toString() + "\n");
			}
			// Close file
			out.close();
		}else {
			System.out.println("File name must only consist of letters, not special characters!");
		}
	}
	
    /**
    	 * constructor for DroneInterface
    	 * sets up scanner used for input and the arena
    	 * then has main loop allowing user to enter commands
     * @throws IOException 
     */
    public DroneInterface() throws IOException {
    	 s = new Scanner(System.in);			// set up scanner for user input
    	 myArena = new DroneArena(20, 6);	// create arena of size 20*6
    	
        char ch = ' ';
        do {
        	System.out.print("Enter (A)dd drone get (I)nformation, (D)isplay canvas, (M)ove all drones once, Move all drones (t)en times, Create a new A(r)ena, (S)tore current building to file, (L)oad building from file or e(X)it > ");
        	ch = s.next().charAt(0);
        	s.nextLine();
        	switch (ch) {
    				case 'A' :
    				case 'a' :
        							myArena.addDrone();	// add a new drone to arena
        							break;
        		case 'I' :
        		case 'i' :
        							System.out.print(myArena.toString());
            					break;
        		case 'D' :
        		case 'd' :
        							doDisplay();
            					break;
        		case 'M' :
        		case 'm' :
				        			moveDronesOneTime();
            					break;
        		case 'T' :
        		case 't' :
        							moveDronesTenTime(10);
            					break;
        		case 'R' :
        		case 'r' :
        							newArena();
            					break;
        		case 'L' :
        		case 'l' :
        							loadArenaMenu();
            					break;
        		case 'S' :
        		case 's' :
        							saveArenaMenu();
            					break;
        		case 'x' : 	ch = 'X';				// when X detected program ends
        							break;
        	}
    		} while (ch != 'X');						// test if end
        
       s.close();									// close scanner
    }
    
	public static void main(String[] args) throws IOException, InterruptedException {
		DroneInterface r = new DroneInterface();	// just call the interface
	}

}