package drone;

public class Drone {
	// Declaration of x,y position of drone
	private int x, y;
	// Declaration of unique identifier of drone
	private static int UID;
	private int tempUID;
	// Declaration of direction of drone
	private Direction direction;
	
	// Getter for x position of drone
	int getposX() {
		return this.x;
	}
	
	// Getter for y position of drone
	int getposY() {
		return this.y;
	}

	// Drone constructor
	public Drone(int x, int y, Direction d) {
		// Initialisation of variables for constructor
		this.x = x;
		this.y = y;
		tempUID = UID++;
		this.direction = d;
	}
	
	// Return true or false boolean method
	public boolean isHere(int sx, int sy) {
		// Return true if x is equal to sx and y is equal to sy
		return this.x == sx && this.y == sy;
	}
	
	// method to call to location of drone on canvas
	public void displayDrone(ConsoleCanvas c) {
		c.showIt(this.x, this.y, 'D');
	}
	
	// toString override returns drone id, position and direction
	public String toString() {
		return "Drone " + this.tempUID +" is at " + this.x +","+this.y+" and the direction is " +this.direction;
	}
	
	// method to see if drone can move to area based on direction and location
    public boolean tryToMove(DroneArena area){
        int tempX = this.x;
        int tempY = this.y;
        // Change coordinates based on direction
        switch(this.direction) {
	        case NORTH:
	        	tempY--;
	        	break;
	        case EAST:
	        	tempX++;
	        	break;
	        case SOUTH:
	        	tempY++;
	        	break;
	        case WEST:
	        	tempX--;
	        	break;
			default:
				break;
        }
        
        // If drone can move at coords
		if (area.canMoveHere(tempX, tempY)) {
			// Change coordinates to new coordinates
			this.x = tempX;
			this.y = tempY;
			return true;
		}else {
			// If not then get next direction from Direction class
			direction = direction.getNext(direction);
			return false;
		}
    }
	
	public static void main(String[] args) {
		Drone d = new Drone(5, 2,Direction.SOUTH);
		System.out.println(d.toString());// print where is}	
	}
}
