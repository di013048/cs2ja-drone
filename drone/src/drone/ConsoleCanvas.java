package drone;

public class ConsoleCanvas {
	// Declaration of 2d array for canvas
	private char[][] canvas;
	// Declaration of xSize and ySize of canvas
	private int xSize, ySize;
	
	// Default constructor for ConsoleCanvas
	public ConsoleCanvas() {
		new ConsoleCanvas(10,5);
	}
	
	// Override ConsoleCanvas constructor
	public ConsoleCanvas(int xSize, int ySize) {
		// Initialises xSize and ySize + 2 to account for border
		this.xSize = xSize+2;
		this.ySize = ySize+2;
		// Initialises canvas
		canvas = new char[xSize+2][ySize+2];
		// Loops through canvas elements, placing # on borders, and nothing inside canvas
		for (int i = 0; i < ySize+2; i++) {
            for (int x = 0; x < xSize+2; x++) {
            	canvas[x][i] = ' ';
            	canvas[0][i] = '#';
        		canvas[x][0] = '#';
        		canvas[xSize+1][i] = '#';
        		canvas[x][ySize+1] = '#';
            }
        }
	}
	
	// Method called to display drone on canvas
	public void showIt(int x, int y, char drone) {
		// Set canvas position to drone character
		if (x != 0 || y != 0 || x != xSize-1 || y != ySize-1) {
			canvas[x+1][y+1] = drone;
		}
	}
	
	// Method to output canvas
	public String toString() {
		// Initialises string to use as a string builder
		String stringReturn = "";
		// Loop through all elements in canvas
		for (int i = 0; i < ySize; i++) {
            for (int x = 0; x < xSize; x++) {
            	// Concatenate new canvas element onto string builder
            	stringReturn = stringReturn + canvas[x][i];
            }
            // Concatenate new line onto string builder
            stringReturn = stringReturn + "\n";
		}
		// Return canvas
		return stringReturn;
	}
	
	// Main
	public static void main(String[] args) {
		ConsoleCanvas c = new ConsoleCanvas (10, 5); // create a canvas
		c.showIt(4,3,'D'); // add a Drone at 4,3S
		System.out.println(c.toString()); // display result
	}

}
