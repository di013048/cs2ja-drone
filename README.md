# cs2ja-drone
## Created by Jamie Morris
A drone simulation created in Java, along with the following functionality.
- Create new arena with specified x and y size
- Add drones to arena
- Move drones in arena
- Move drones in arena 10 times
- Display drone arena
- List all drones and their information
- Store drone arena in file
- Load drone arena from file
- Exit