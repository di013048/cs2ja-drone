package drone;

import java.util.Random;
import javax.swing.JFileChooser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class DroneArena {
	// Initialisation of drone array list  
	ArrayList<Drone> drones = new ArrayList<>();
	// Declaration of xSize and ySize of canvas
	private int xSize, ySize;
	// Declaration of 2d array for arena
	private int[][] arena;
	
	// Getter for arena size x
	public int getxSize() {
	    return xSize;
	}
	
	// Getter for arena size y
	public int getySize() {
	    return ySize;
	}
	
	// Getter for arena
	public int[][] getArena() {
		return arena;
	}
	

	public void droneAdder(String adder) {
		String[] splitted = adder.split(",");
		System.out.println(adder);
		int x = Integer.parseInt(splitted[0]);
		int y = Integer.parseInt(splitted[1]);
		
		drones.add(x,y, Direction.getRandomDirection());
	}
	
	// Constructor to create arena
	public DroneArena() {
		new DroneArena(10, 20);
	}
	
	// Constructor override to allow variable sized arena
	public DroneArena(int xSize, int ySize) {
		// Initialisation of variables for constructor
		this.xSize = xSize;
		this.ySize = ySize;
		arena = new int[xSize][ySize];
		
		// Loop through arena element
		for (int i = 0; i < ySize; i++) {
            for (int x = 0; x < xSize; x++) {
            	// Initialise current loop element to 0
            	arena[x][i] = 0;
            }
        }
	}
	
	// Method to check space availability
	public Drone getDroneAt(int x, int y) {
		// Loop through drones list
		for (Drone element : drones) {
			// Check availability
			if(element.isHere(x, y)) {
				// return element
				return element;
			}
		}
		// Return null
		return null;
	}
	
	// Method to add drone to centre of arena
	public void addDroneToCenter() {
		// Initialise object to create drone in centre of arena
		Drone middle = new Drone(this.xSize/2, this.ySize/2, Direction.getRandomDirection());
		// Call to method with middle drone area values
		drones.add(middle);
	}
	
	// Method to add drone to arena
	public void addDrone() {
		// If drone list size is less than total arena size
		if(drones.size() < xSize*ySize) {
			// Declare random x and y
			int xRand, yRand;
			// Declare random direction
			Direction randomDirection;
			// Declare random generator object
			Random randomGenerator;
			// Initialise random number using random object
			randomGenerator = new Random();
			// Do while drone is already there
			do {
				// Initialise random number using random object
				randomGenerator = new Random();
				// Initialise x as random numbers, within arena x size bounds
				xRand = randomGenerator.nextInt(xSize);
				// Initialise y as random numbers, within arena y size bounds
				yRand = randomGenerator.nextInt(ySize);
				// Initialise random direction calling to random direction method from Direction object
				randomDirection = Direction.getRandomDirection();
			}while(getDroneAt(xRand, yRand) != null && xRand < this.xSize-1 && yRand < this.ySize-1 && xRand == 0 && yRand == 0);
			// Initialise element as 1
			arena[xRand][yRand] = 1;
			// Add drone to drone list
			drones.add(new Drone(xRand, yRand, randomDirection));
		}else {
			System.out.println("Arena full");
		}
	}
	
	// toString override to print out of information of drones
	public String toString() {
		// Initialises empty string
		String returnString = "";
		// Loops through drones array list
		for( Drone drone : drones ) {
			// Concatenates return string with drone information and new line
			returnString = returnString + drone + "\n";
	     }
		// Return string
		return returnString;
	}
	
	// Method to show drones
	public void showDrones(ConsoleCanvas c) {
		for (Drone element : drones) {
			element.displayDrone(c);
		}
	}
	
	// Boolean Method to check if a drone can be moved into parsed position
	public boolean canMoveHere(int x, int y) {
		// If x and y is not in the border, and they are both less than arena size and there isn't a drone already there
//		xRand < this.xSize-1 && yRand < this.ySize-1 && xRand == 0 && yRand == 0
		if (x > -1 && y > -1 && x < xSize && y < ySize && getDroneAt(x,y) == null) {
			// Return true
			return true;
		}else {
			// Return false
			return false;
		}
	}
	
	public void moveAllDrones(DroneArena myArena) {
		for (Drone element : drones) {
			element.tryToMove(this);
		}
	}
	
	public static void main(String[] args){
		DroneArena a = new DroneArena(10, 5); // create drone arena
		a.addDrone();
		a.addDrone();
		a.addDrone();
		a.addDrone();
		a.addDrone();
		a.addDrone();
		System.out.println(a.toString()); // print where is
		
	}
}
